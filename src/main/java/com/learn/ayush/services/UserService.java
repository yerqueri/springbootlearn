package com.learn.ayush.services;

import java.util.List;

import com.learn.ayush.models.Users;

public interface UserService {
	
	public List<Users> findAllUsers();
	public Users findUserById(Integer id);
	public Users addNewUser(Users user);
	public Users removeUserById(Integer id);
}
