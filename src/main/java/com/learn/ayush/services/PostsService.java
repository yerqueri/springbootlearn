package com.learn.ayush.services;

import java.util.List;

import javax.validation.Valid;

import org.springframework.http.ResponseEntity;

import com.learn.ayush.models.Posts;

public interface PostsService {

	List<Posts> getAllPosts();

	Posts getPostsById(Integer id);

	Posts createNewPost(@Valid Posts post);

}
