package com.learn.ayush.services;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import com.learn.ayush.models.Users;


public interface UserJpaService{

	List<Users> findAllUsers();

	Optional<Users> findUserById(Integer id);

	Users AddNewUser(@Valid Users user);

	void deleteById(Integer id);


}
