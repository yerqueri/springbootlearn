package com.learn.ayush.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.learn.ayush.models.Name;
import com.learn.ayush.models.PersonV1;
import com.learn.ayush.models.PersonV2;

@RestController
public class PersonVersioningController {
	
	//URL versioning
	
	@GetMapping(path="v1/person")
	public PersonV1 getPersonV1() {
		return new PersonV1("Ayush Yadav");
	}
	
	@GetMapping(path="v2/person")
	public PersonV2 getPersonV2() {
		return new PersonV2(new Name("Ayush","Yadav"));
	}
	
	//Request param versioning
	
	@GetMapping(path="person",params="version=1")
	public PersonV1 getPersonParamV1() {
		return new PersonV1("Ayush Yadav");
	}
	
	@GetMapping(path="person",params="version=2")
	public PersonV2 getPersonParamV2() {
		return new PersonV2(new Name("Ayush","Yadav"));
	}
	
	//custom header versioning- makes caching difficult
	
	@GetMapping(path="person",headers="X_API_VERSION=1")
	public PersonV1 getPersonheaderV1() {
		return new PersonV1("Ayush Yadav");
	}
	
	@GetMapping(path="person",headers="X_API_VERSION=2")
	public PersonV2 getPersonheaderV2() {
		return new PersonV2(new Name("Ayush","Yadav"));
	}
	
	//MIME or Accept Header Versioning- makes caching difficult
	
	@GetMapping(path="person",produces="application/vnd.company.app-v1+json")
	public PersonV1 producesv1() {
		return new PersonV1("Ayush Yadav");
	}
	
	@GetMapping(path="person",produces="application/vnd.company.app-v2+json")
	public PersonV2 producesv2() {
		return new PersonV2(new Name("Ayush","Yadav"));
	}


}
