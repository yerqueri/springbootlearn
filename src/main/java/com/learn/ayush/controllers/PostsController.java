package com.learn.ayush.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.learn.ayush.models.Posts;
import com.learn.ayush.services.PostsService;

@RestController
public class PostsController {
	@Autowired
	private PostsService postsService;
	
	@GetMapping(path="posts")
	List<Posts> getAllPosts() {
		return postsService.getAllPosts();
	}
	
	@GetMapping(path="posts/{id}")
	Posts findPostbyId(@PathVariable Integer id){
		Posts post= postsService.getPostsById(id);
		if(post==null) {
			throw new ResourceNotFoundException();
		}
		
		return post;
	}
}
