package com.learn.ayush.controllers;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.http.converter.json.MappingJacksonValue;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.ser.FilterProvider;
import com.fasterxml.jackson.databind.ser.impl.SimpleBeanPropertyFilter;
import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider;
import com.learn.ayush.models.SomeBean;

@RestController
public class FilteringController {

	@GetMapping(path = "some")
	public MappingJacksonValue getSomeBean() {
		SomeBean someBean = new SomeBean("val1", "val2", "val3");

		SimpleBeanPropertyFilter filter = SimpleBeanPropertyFilter.filterOutAllExcept("feild1", "feild2");
		FilterProvider filters = new SimpleFilterProvider().addFilter("SomeBeanFilter", filter);
		MappingJacksonValue mapping = new MappingJacksonValue(someBean);
		mapping.setFilters(filters);
		return mapping;
	}

	@GetMapping(path = "somemore")
	public MappingJacksonValue getSomeMore() {
		List<SomeBean> sblist = Arrays.asList(new SomeBean("val1", "val2", "val3"),
				new SomeBean("val11", "val12", "val13"));
		
		MappingJacksonValue mapping = new MappingJacksonValue(sblist);
		Set<String> fieldsNotToFilter =new HashSet<>();
		fieldsNotToFilter.addAll(Arrays.asList("feild2","feild3"));
		mapping.setFilters(getMappigFilters("SomeBeanFilter", fieldsNotToFilter));
		return mapping;
	}
	
	FilterProvider getMappigFilters(String filtername,Set<String> fieldsNotToFilter){
		SimpleBeanPropertyFilter filter=SimpleBeanPropertyFilter.filterOutAllExcept(fieldsNotToFilter);
		FilterProvider filters=new SimpleFilterProvider().addFilter("SomeBeanFilter", filter);
		return filters;
	}

}
