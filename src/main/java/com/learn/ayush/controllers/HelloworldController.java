package com.learn.ayush.controllers;

import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import com.learn.ayush.models.HelloWorldBean;

@RestController
public class HelloworldController {
	
	@Autowired
	private MessageSource messageSource;
	
	@GetMapping(path="/helloworld")
	public String getGreeting(@RequestHeader(name="Accept-Language",required=false) Locale locale) {
		return messageSource.getMessage("hello.message",null,locale);
	}
	
	@GetMapping(path="/helloworld-bean")
	public HelloWorldBean getGreetingBean(@RequestHeader(name="Accept-Language",required=false) Locale locale) {
		return new HelloWorldBean(messageSource.getMessage("hello.message",null,locale));
	}
	
	@GetMapping(path="/helloworld/{name}")
	public HelloWorldBean getGreetingBean(@PathVariable String name) {
		return new HelloWorldBean(messageSource.getMessage("hello.message",null,LocaleContextHolder.getLocale())+" "+name);
	}

}
