package com.learn.ayush.controllers;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.*;

import java.net.URI;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.mvc.ControllerLinkBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.learn.ayush.exceptions.ResourceNotFoundException;
import com.learn.ayush.models.Users;
import com.learn.ayush.services.UserService;

@RestController
public class UserController {

	@Autowired
	@Qualifier("userserviceimpl")
	private UserService userService;

	@GetMapping(path = "users")
	List<Users> retriveAllUsers() {
		return userService.findAllUsers();
	}

	@GetMapping(path = "users", params = "id")
	Resource<Users> retriveUserById(@RequestParam(value = "id") Integer id) {
		Users user =userService.findUserById(id);
		if(user==null) {
			throw new ResourceNotFoundException("id :"+id);
		}
		Resource<Users> resource = new Resource<Users>(user);
		ControllerLinkBuilder link=linkTo(methodOn(this.getClass()).retriveAllUsers());
		resource.add(link.withRel("all-users"));
		return resource;
	}

	@GetMapping(path = "users/{id}")
	Resource<Users> retriveUserByIdVar(@PathVariable Integer id) {
		Users user =userService.findUserById(id);
		if(user==null) {
			throw new ResourceNotFoundException("id :"+id);
		}
		Resource<Users> resource = new Resource<Users>(user);
		ControllerLinkBuilder link=linkTo(methodOn(this.getClass()).retriveAllUsers());
		resource.add(link.withRel("all-users"));
		return resource;
	}

	@PostMapping(path="users")
	ResponseEntity<Object> addNewUser(@Valid @RequestBody(required=true) Users user) {
		Users savedUser =userService.addNewUser(user);
		URI lokation =ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(savedUser.getId()).toUri();
		return ResponseEntity.created(lokation).build();
	}

	@DeleteMapping(path = "users/delete",params="id")
	ResponseEntity<Object> deleteUserById(@RequestParam(value = "id", required = true) Integer id) {
		Users user =userService.removeUserById(id);
		if(user==null) {
			return ResponseEntity.noContent().build();
		}
		return ResponseEntity.ok().build();
	}

}
