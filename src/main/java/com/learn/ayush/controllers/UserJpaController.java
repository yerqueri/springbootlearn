package com.learn.ayush.controllers;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import java.net.URI;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.mvc.ControllerLinkBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.learn.ayush.exceptions.ResourceNotFoundException;
import com.learn.ayush.models.Posts;
import com.learn.ayush.models.Users;
import com.learn.ayush.services.PostsService;
import com.learn.ayush.services.UserJpaService;
import com.learn.ayush.services.UserService;

@RestController
public class UserJpaController {
	@Autowired
	//@Qualifier("userserviceimpl")
	private UserJpaService userService;
	
	@Autowired
	private PostsService postService;

	@GetMapping(path = "jpa/users")
	List<Users> retriveAllUsers() {
		return userService.findAllUsers();
	}

	@GetMapping(path = "jpa/users", params = "id")
	Resource<Users> retriveUserById(@RequestParam(value = "id") Integer id) {
		Optional<Users> user =userService.findUserById(id);
		if(!user.isPresent()) {
			throw new ResourceNotFoundException("id :"+id);
		}
		Resource<Users> resource = new Resource<Users>(user.get());
		ControllerLinkBuilder link=linkTo(methodOn(this.getClass()).retriveAllUsers());
		resource.add(link.withRel("all-users"));
		return resource;
	}

	@GetMapping(path = "jpa/users/{id}")
	Resource<Users> retriveUserByIdVar(@PathVariable Integer id) {
		Optional<Users> user =userService.findUserById(id);
		if(!user.isPresent()) {
			throw new ResourceNotFoundException("id :"+id);
		}
		Resource<Users> resource = new Resource<Users>(user.get());
		ControllerLinkBuilder link=linkTo(methodOn(this.getClass()).retriveAllUsers());
		resource.add(link.withRel("all-users"));
		return resource;
	}

	@PostMapping(path="jpa/users")
	ResponseEntity<Object> addNewUser(@Valid @RequestBody(required=true) Users user) {
		Users savedUser =userService.AddNewUser(user);
		URI lokation =ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(savedUser.getId()).toUri();
		return ResponseEntity.created(lokation).build();
	}

	@DeleteMapping(path = "jpa/users/delete",params="id")
	void deleteUserById(@RequestParam(value = "id", required = true) Integer id) {
		userService.deleteById(id);
	}
	
	@GetMapping(path="jpa/users/{id}/posts")
	List<Posts> findPostsByUserId(@PathVariable Integer id){
		Optional<Users> user =userService.findUserById(id);
		if(!user.isPresent()) {
			throw new ResourceNotFoundException("id :"+id);
		}
		return user.get().getPosts();
	}
	
	@PostMapping(path="jpa/users/{id}/posts")
	ResponseEntity<Object> addNewPostForUser(@Valid @RequestBody(required=true) Posts post,@PathVariable Integer id) {
		Optional<Users> user =userService.findUserById(id);
		if(!user.isPresent()) {
			throw new ResourceNotFoundException("id :"+id);
		}
		post.setUser(user.get());
		Posts postedPost=postService.createNewPost(post);
		URI lokation =ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(postedPost.getId()).toUri();
		return ResponseEntity.created(lokation).build();
	}
}
