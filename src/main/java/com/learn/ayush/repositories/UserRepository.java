package com.learn.ayush.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.learn.ayush.models.Users;

public interface UserRepository extends JpaRepository<Users, Integer>{
	
}
