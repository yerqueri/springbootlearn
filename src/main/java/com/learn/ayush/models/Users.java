package com.learn.ayush.models;

import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.validation.constraints.Past;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

//@JsonIgnoreProperties(value= {"id","name"})
@ApiModel(description = "Information Regarding Users")
@Entity
public class Users {
	
	//@JsonIgnore
	@Id
	@GeneratedValue
	private Integer id;
	
	@Size(min = 2, message = "name should contain at least 2 characters")
	@ApiModelProperty(notes = "Name must contain at least 2 chars")
	private String name;
	
	
	@Past(message = "Date should be in Past")
	@ApiModelProperty(notes = "BirthDate should be in the past")
	@JsonProperty(value="date_of_Birth")
	private Date dob;
	
	@JsonManagedReference
	@OneToMany(mappedBy="user")
	private List<Posts> posts;

	public Users() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Users(Integer id, @Size(min = 2, message = "name should contain at least 2 characters") String name,
			@Past(message = "Date should be in Past") Date dob, List<Posts> posts) {
		super();
		this.id = id;
		this.name = name;
		this.dob = dob;
		this.posts = posts;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getDob() {
		return dob;
	}

	public void setDob(Date dob) {
		this.dob = dob;
	}

	public List<Posts> getPosts() {
		return posts;
	}

	public void setPosts(List<Posts> posts) {
		this.posts = posts;
	}
	
	
}
