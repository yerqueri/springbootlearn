package com.learn.ayush.models;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
public class Posts {
	
	@Id
	@GeneratedValue
	Integer id;
	
	@JsonBackReference
	@ManyToOne(fetch=FetchType.LAZY)
	Users user;
	
	String summary;
	
	String content;
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Users getUser() {
		return user;
	}

	public void setUser(Users user) {
		this.user = user;
	}

	public String getSummary() {
		return summary;
	}

	public void setSummary(String summary) {
		this.summary = summary;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Posts() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Posts(Integer id, Users user, String summary, String content) {
		super();
		this.id = id;
		this.user = user;
		this.summary = summary;
		this.content = content;
	}

	@Override
	public String toString() {
		return "Posts [id=" + id + ", summary=" + summary + ", content=" + content + "]";
	}

}
