package com.learn.ayush.models;

public class Name {
	
	String firstName;
	String lastName;
	
	

	public Name(String name, String lastName) {
		super();
		this.firstName = name;
		this.lastName = lastName;
	}



	public String getFirstName() {
		return firstName;
	}



	public void setFirstName(String name) {
		this.firstName = name;
	}



	public String getLastName() {
		return lastName;
	}



	public void setLastName(String lastName) {
		this.lastName = lastName;
	}



	public Name() {
		// TODO Auto-generated constructor stub
	}

}
