package com.learn.ayush.models;


public class HelloWorldBean {
	
	String message;
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public HelloWorldBean(String message) {
		super();
		this.message = message;
	}

	public HelloWorldBean() {
		// TODO Auto-generated constructor stub
	}


}
