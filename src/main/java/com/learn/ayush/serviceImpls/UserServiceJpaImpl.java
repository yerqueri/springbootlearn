package com.learn.ayush.serviceImpls;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.learn.ayush.models.Users;
import com.learn.ayush.repositories.UserRepository;
import com.learn.ayush.services.UserJpaService;

@Service
public class UserServiceJpaImpl implements UserJpaService{
	
	@Autowired
	private UserRepository userRepo;

	@Override
	public List<Users> findAllUsers() {
		return userRepo.findAll();
	}

	@Override
	public Optional<Users> findUserById(Integer id) {
		return userRepo.findById(id);
	}

	@Override
	public Users AddNewUser(@Valid Users user) {
		return userRepo.save(user);
	}

	@Override
	public void deleteById(Integer id) {
		userRepo.deleteById(id);
	}
}
