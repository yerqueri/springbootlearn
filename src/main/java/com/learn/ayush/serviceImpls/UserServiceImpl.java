package com.learn.ayush.serviceImpls;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.learn.ayush.models.Users;
import com.learn.ayush.services.UserService;

@Service
@Qualifier("userserviceimpl")
public class UserServiceImpl implements UserService {
	
	private static List<Users> userList = new ArrayList<>();
	private static Integer userCount=3;
	
	static {
		userList.add(new Users(1,"Ayush",new Date(),null));
		userList.add(new Users(2,"Aman",new Date(),null));
		userList.add(new Users(3,"Yerqueri",new Date(),null));
	}

	@Override
	public List<Users> findAllUsers() {
		return userList;
	}

	@Override
	public Users findUserById(Integer id) {
		for(Users user:userList) {
			if(user.getId().equals(id)) {
				return user;
			}
		}
		return null;
	}

	@Override
	public Users addNewUser(Users user) {
		userCount++;
		user.setId(userCount);
		userList.add(user);
		return user;
	}

	@Override
	public Users removeUserById(Integer id) {
		for(int i=0;i<userList.size();i++) {
			if(userList.get(i).getId().equals(id)) {
				Users user =userList.remove(i);
				userCount--;
				return user;
			}
		}
		return null;
	}

}
