package com.learn.ayush.serviceImpls;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.learn.ayush.models.Posts;
import com.learn.ayush.repositories.PostsRepository;
import com.learn.ayush.services.PostsService;

@Service
public class PostsServiceImpl implements PostsService{
	
	@Autowired
	private PostsRepository postsRepository;
	
	@Override
	public List<Posts> getAllPosts() {
		return postsRepository.findAll();
	}

	@Override
	public Posts getPostsById(Integer id) {
		return postsRepository.findById(id).get();
	}

	@Override
	public Posts createNewPost(Posts post) {
		return postsRepository.save(post);
	}

}
