insert into users values(101,sysdate(),'u1');
insert into users values(102,sysdate(),'u2');
insert into users values(103,sysdate(),'u3');

insert into posts values(1001,'Some Content','Some summary',101);
insert into posts values(1002,'Some Content','Some summary',101);
insert into posts values(1003,'Some Content','Some summary',101);

insert into posts values(2001,'Some Content','Some summary',102);
insert into posts values(2002,'Some Content','Some summary',102);
insert into posts values(2003,'Some Content','Some summary',102);

insert into posts values(3001,'Some Content','Some summary',103);
insert into posts values(3002,'Some Content','Some summary',103);
insert into posts values(3003,'Some Content','Some summary',103);